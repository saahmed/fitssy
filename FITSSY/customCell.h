//
//  customCell.h
//  iChat
//
//  Created by iMac on 24/04/2015.
//  Copyright (c) 2015 FullCircle GeoSocial Network, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface customCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *chatterNameLabel;

@end
