//
//  Singleton.h
//  FITSSY
//
//  Created by iMac on 29/04/2015.
//  Copyright (c) 2015 FullCircle GeoSocial Network, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Singleton : NSObject

+ (id)sharedManager;

@end
