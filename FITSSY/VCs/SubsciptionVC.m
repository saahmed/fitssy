//
//  SubsciptionVC.m
//  FITSSY
//
//  Created by iMac on 29/04/2015.
//  Copyright (c) 2015 FullCircle GeoSocial Network, Inc. All rights reserved.
//

#import "SubsciptionVC.h"
#import "subCell.h"

@interface SubsciptionVC () <UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) NSMutableArray *usersArray;

@property (weak, nonatomic) IBOutlet UITableView *tableview;
@end

@implementation SubsciptionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _usersArray  = [[NSMutableArray alloc]  init];
    [_usersArray addObject:@"Trainer Rehan"];
    [_usersArray addObject:@"Trainer Farhan"];
    [_usersArray addObject:@"Trainer Rizwan"];
    [_usersArray addObject:@"Trainer Kamran"];
    
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton = NO;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _usersArray.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    subCell *cell = [tableView dequeueReusableCellWithIdentifier:@"subCell"];
    if (!cell) {
        [_tableview registerNib:[UINib nibWithNibName:@"subCell" bundle:nil] forCellReuseIdentifier:@"subCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"subCell"];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(subCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.chatterNameLabel.text = [NSString stringWithFormat:@"%@",[_usersArray objectAtIndex:indexPath.row]];
}

@end
