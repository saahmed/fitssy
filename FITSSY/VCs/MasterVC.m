//
//  MasterVC.m
//  FITSSY
//
//  Created by iMac on 29/04/2015.
//  Copyright (c) 2015 FullCircle GeoSocial Network, Inc. All rights reserved.
//

#import "MasterVC.h"
#import <ParseFacebookUtilsV4/PFFacebookUtils.h>
#import <Parse/Parse.h>
#import "ListVC.h"
#import "SubsciptionVC.h"

@interface MasterVC ()

@end

@implementation MasterVC



- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    self.navigationItem.hidesBackButton = YES;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)goToCategoryVC:(id)sender {
    [self.navigationController pushViewController:[ListVC new] animated:YES];
}

- (IBAction)goToSubscriptionVC:(id)sender {
    [self.navigationController pushViewController:[SubsciptionVC new] animated:YES];
    
}

- (IBAction)logoutPressed:(id)sender {
    [PFUser logOut];
    [self.navigationController popViewControllerAnimated:YES];
}
@end
