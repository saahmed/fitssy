//
//  LoginVC.m
//  FITSSY
//
//  Created by iMac on 29/04/2015.
//  Copyright (c) 2015 FullCircle GeoSocial Network, Inc. All rights reserved.
//

#import "LoginVC.h"
#import "MasterVC.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <ParseFacebookUtilsV4/PFFacebookUtils.h>
#import <Parse/Parse.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>



@interface LoginVC ()
@property (weak, nonatomic) IBOutlet UIImageView *logoImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoImageHeightConstaint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoImageWidthConstaint;
@property (weak, nonatomic) IBOutlet UIButton *fbLoginButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *fbLoginButtonWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *fbLoginButtonHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *weWontPostLabel;

@end

@implementation LoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (NSInteger)screenSize {
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.height == 480)
        {
            return 4;
        }
        if(result.height == 568)
        {
            return 5;
        }
        if(result.height == 667)
        {
            return 6;
        }
        if(result.height == 736)
        {
            return 66;
        }
    }
    return 0;
}


- (void)viewWillAppear:(BOOL)animated {

    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    
    if([self screenSize] == 4) {
        _fbLoginButtonHeightConstraint.constant = 50* 0.851;
        _fbLoginButtonWidthConstraint.constant = 306 *0.851;
        _logoImageHeightConstaint.constant = 290 *0.851;
        _logoImageWidthConstaint.constant = 214*0.851;
    }
    else {
        NSLog(@"tum koi or ho");
    }
    
    
    //Uncomment if you want to login automatically for facebook
    if ([PFUser currentUser] && // Check if user is cached
        [PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]) { // Check if user is linked to Facebook

        [self.navigationController pushViewController:[MasterVC new] animated:YES];

    }

}

- (IBAction)FBLoginButtonPressed:(id)sender {
    // Do some Random Stuff
    [self _loginWithFacebook];
}

-(void)_loginWithFacebook {
    // Set permissions required from the facebook user account
    NSArray *permissionsArray = @[ @"user_about_me", @"user_relationships", @"user_birthday", @"user_location"];
    
    // Login PFUser using Facebook
    [PFFacebookUtils logInInBackgroundWithReadPermissions:permissionsArray block:^(PFUser *user, NSError *error) {
        if (!user) {
            NSLog(@"Uh oh. The user cancelled the Facebook login.");
            NSLog(@"User data %@", user);
        } else if (user.isNew) {
            [self.navigationController pushViewController:[MasterVC new] animated:YES];
            
            NSLog(@"User signed up and logged in through Facebook!");
            NSLog(@"User data %@", user);
            
        } else {
            NSLog(@"User logged in through Facebook!");
            [self.navigationController pushViewController:[MasterVC new] animated:YES];
            
            NSLog(@"User data %@", user);
        }
    }];
}

- (void)_loadData {
    
    FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil];
    [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
        if (!error) {
            // result is a dictionary with the user's Facebook data
            //            NSDictionary *userData = (NSDictionary *)result;
            
            //            NSString *facebookID = userData[@"id"];
            //            NSString *name = userData[@"name"];
            //            NSString *location = userData[@"location"][@"name"];
            //            NSString *gender = userData[@"gender"];
            //            NSString *birthday = userData[@"birthday"];
            //            NSString *relationship = userData[@"relationship_status"];
            //
            //            NSURL *pictureURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large&return_ssl_resources=1", facebookID]];
        }
        
    }];
}
@end
