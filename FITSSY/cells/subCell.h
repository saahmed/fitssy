//
//  subCell.h
//  FITSSY
//
//  Created by iMac on 29/04/2015.
//  Copyright (c) 2015 FullCircle GeoSocial Network, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface subCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *chatterNameLabel;
@end
