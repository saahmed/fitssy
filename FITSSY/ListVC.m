//
//  ListVC.m
//  iChat
//
//  Created by iMac on 28/04/2015.
//  Copyright (c) 2015 FullCircle GeoSocial Network, Inc. All rights reserved.
//

#import "ListVC.h"
#import "customCell.h"



@interface ListVC () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *usersArray;

@end

@implementation ListVC

- (void)viewDidLoad {
    [super viewDidLoad];

    _usersArray  = [[NSMutableArray alloc]  init];
    [_usersArray addObject:@"Trainer Rehan"];
    [_usersArray addObject:@"Trainer Farhan"];
    [_usersArray addObject:@"Trainer Rizwan"];
    [_usersArray addObject:@"Trainer Kamran"];
    
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton = NO;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _usersArray.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    customCell *cell = [tableView dequeueReusableCellWithIdentifier:@"customCell"];
    if (!cell) {
        [_tableView registerNib:[UINib nibWithNibName:@"customCell" bundle:nil] forCellReuseIdentifier:@"customCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"customCell"];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(customCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.chatterNameLabel.text = [NSString stringWithFormat:@"%@",[_usersArray objectAtIndex:indexPath.row]];
}

@end
